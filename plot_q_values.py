import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":
    for learner in ["q-learning", "friend-q", "foe-q", "correlated-q"]:
        q_values = np.load(learner + ".npy")
        plt.plot(q_values[:,0], q_values[:,1])
        plt.xlabel("Simulaion Iteration")
        plt.ylabel("Q-value Difference")
        plt.xlim([0, 10e5])
        plt.ylim([0, 0.5])
        plt.savefig(learner + ".png", bbox_inches="tight")
        plt.close()
