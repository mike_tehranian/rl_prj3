import numpy as np
from cvxopt import matrix, solvers
import time
import random


NUM_PLAYERS = 2
NUM_ACTIONS = 5
SINGLE_ACTION = 'single'
JOINT_ACTION = 'joint'
# Defined from Greenwald-Hall(2003)
NUM_ITERATIONS = 10e5

class SoccerEnvironment(object):
    def __init__(self):
        self.players = list(range(NUM_PLAYERS))

        self.game_grid = tuple((x, y) for x in range(2) for y in range(4))
        self.initial_states = tuple((x,y) for x in range(2) for y in range(1,3))
        self.states = tuple((p0, p1, b) for p0 in self.game_grid for p1 in self.game_grid
                                    for b in self.players if p0 != p1)

        self.player_positions = None
        self.player_with_ball = None

    def get_opponent(self, player):
        return np.abs(player-1)

    def get_game_state(self):
        return (self.player_positions[0], self.player_positions[1], self.player_with_ball)

    def get_actions_per_player(self, random_players, random_actions):
        if random_players[0] == 0:
            player_actions = (random_actions[0], random_actions[1])
        else:
            player_actions = (random_actions[1], random_actions[0])
        return player_actions

    def attempt_move(self, player, action):
        current_position = self.player_positions[player]
        opponent = self.get_opponent(player)

        # ACTIONS = ["N", "S", "E", "W", "Stick"]
        if action == 0:
            new_position = (current_position[0] - 1, current_position[1])
        elif action == 1:
            new_position = (current_position[0] + 1, current_position[1])
        elif action == 2:
            new_position = (current_position[0], current_position[1] + 1)
        elif action == 3:
            new_position = (current_position[0], current_position[1] - 1)
        elif action == 4:
            new_position = (current_position[0], current_position[1])
        else:
            raise ValueError("Invalid Action")

        if new_position not in self.game_grid:
            # Hit a wall, do not move
            return

        if self.player_positions[opponent] == new_position:
            # Collision so do not move
            if self.player_with_ball == player:
                # If have ball, give it to the opponent
                self.player_with_ball = opponent
        else:
            # If no collisions free move to it
            self.player_positions[player] = new_position

    def get_reward_state(self):
        if self.players[0] == self.player_with_ball:
            position = self.player_positions[0]
            if position[1] == 0:
                return (100, -100)
            elif position[1] == 3:
                return (-100, 100)
        elif self.players[1] == self.player_with_ball:
            position = self.player_positions[1]
            if position[1] == 0:
                return (100, -100)
            elif position[1] == 3:
                return (-100, 100)

        # Default reward vector -- no agent scores
        return (0, 0)

class GameStateLogger(object):
    def __init__(self, solver_type):
        self.num_iterations = 0

        self.iterations = []
        self.q_values = []

        self.solver_type = solver_type
        self.state_logging = ((0, 2), (0, 1), 1)
        if solver_type == SINGLE_ACTION:
            self.s_action = 1
        elif solver_type == JOINT_ACTION:
            self.s_action = (1, 4)

    def add_observation(self, q_value, state_s, action_state):
        if self.solver_type == SINGLE_ACTION:
            action_state_check = action_state[0]
        elif self.solver_type == JOINT_ACTION:
            action_state_check = action_state

        if (state_s == self.state_logging) and (action_state_check == self.s_action):
            # Player A Q-value
            q_value_log = q_value[0]
            self.q_values.append(q_value_log)
            self.iterations.append(self.num_iterations)

class BaseAgentLearner(object):
    def __init__(self):
        # Gamma from Littman 1994 paper
        self.gamma = 0.9

        # Initial learning rate
        self.alpha = 0.2

    def decay_alpha(self):
        self.alpha *= 0.99999
        # Final alpha of 1e-3 from Greenwald-Hall paper
        self.alpha = max(1e-3, self.alpha)

class MultiAgentSolver(BaseAgentLearner):
    def __init__(self, soccer_env):
        super().__init__()
        self.Q = {}
        for s in soccer_env.states:
            # State, ACTIONS x ACTIONS matrix
            self.Q[s] = np.zeros((NUM_ACTIONS, NUM_ACTIONS, 2), dtype=np.float64)

        self.logger = GameStateLogger(JOINT_ACTION)

    def policy_at_log_state(self):
        # Player A's policy matrix
        return self.Q[self.logger.state_logging][:,:,0]

    def update_Q_values(self, state_s, actions, rewards, state_s_prime):
        q_s_a = self.Q[state_s][actions]
        try:
            v_s_prime = self.value_at_state(state_s_prime)
        except ValueError:
            # If numerical error, just use existing value
            v_s_prime = self.Q[state_s_prime][actions]

        for i in range(NUM_PLAYERS):
            q_update = (1.0 - self.alpha) * q_s_a[i] + \
                self.alpha *  ((1.0 - self.gamma) * rewards[i] + self.gamma * v_s_prime[i])
            self.Q[state_s][actions][i] = q_update

        return self.Q[state_s][actions]

class FoeQ(MultiAgentSolver):
    def __init__(self, soccer_env):
        super().__init__(soccer_env)
        self.file_output = "foe-q"

    def value_at_state(self, state):
        Q0 = self.Q[state][:,:,0]
        Q1 = self.Q[state][:,:,1]

        value_col = np.ones((NUM_ACTIONS, 1))

        G0 = np.hstack((value_col, -Q0.T))
        G1 = np.hstack((value_col, -Q1))

        # probability zero constraints
        zero_constraints = np.hstack((np.zeros((NUM_ACTIONS, 1)), -np.eye(NUM_ACTIONS)))

        G0 = np.vstack((G0, zero_constraints))
        G1 = np.vstack((G1, zero_constraints))

        h = np.zeros((G0.shape[0], 1))

        # probability sum constraints: Ax = b
        A = [0] + [1 for _ in range(NUM_ACTIONS)]
        A = np.matrix(A, dtype=np.float64)
        b = np.matrix(1, dtype=np.float64)

        c = [-1] + [0 for _ in range(NUM_ACTIONS)]
        c = np.array(c, dtype=np.float64)

        sol0 = solvers.lp(matrix(c), matrix(G0), matrix(h), matrix(A), matrix(b), solver="glpk")
        sol1 = solvers.lp(matrix(c), matrix(G1), matrix(h), matrix(A), matrix(b), solver="glpk")

        # First variable is the value for each player
        return sol0["x"][0], sol1["x"][0]

class CorrelatedQ(MultiAgentSolver):
    def __init__(self, soccer_env):
        super().__init__(soccer_env)
        self.file_output = "correlated-q"

    def value_at_state(self, state):
        Q_0 = self.Q[state][:,:,0]
        Q_1 = self.Q[state][:,:,1]

        G = []
        rationality_constraints = np.empty((NUM_ACTIONS, NUM_ACTIONS))

        for row_action_i in range(NUM_ACTIONS):
            for row_action_j in range(NUM_ACTIONS):
                if row_action_i != row_action_j:
                    rationality_constraints.fill(0)
                    row_diff = Q_0[row_action_j, :] - Q_0[row_action_i, :]
                    rationality_constraints[row_action_i, :] = row_diff
                    G += [rationality_constraints.ravel().tolist()]

        for col_action_i in range(NUM_ACTIONS):
            for col_action_j in range(NUM_ACTIONS):
                if col_action_i != col_action_j:
                    rationality_constraints.fill(0)
                    col_diff = Q_1[:, col_action_j] - Q_1[:, col_action_i]
                    rationality_constraints[:, col_action_i] = col_diff
                    G += [rationality_constraints.ravel().tolist()]

        G = np.matrix(G, dtype=np.float64)
        G = np.vstack((G, -np.eye(NUM_ACTIONS * NUM_ACTIONS)))
        h = np.zeros((G.shape[0], 1))

        A = np.ones((1, NUM_ACTIONS * NUM_ACTIONS))
        b = np.matrix(1, dtype=np.float64)

        c = -np.ravel(np.sum(self.Q[state], axis = 2))

        sol = solvers.lp(matrix(c), matrix(G), matrix(h), matrix(A), matrix(b), solver=None)
        probabilities = np.array(sol["x"])

        # Take expectation of payoffs to find values
        player0_value = np.sum(-probabilities * Q_0.ravel())
        player1_value = np.sum(-probabilities * Q_1.ravel())

        return player0_value, player1_value

class FriendQ(MultiAgentSolver):
    def __init__(self, soccer_env):
        super().__init__(soccer_env)
        self.file_output = "friend-q"

    def value_at_state(self, state):
        # Naively select the best action without predicting the opponent's move
        player0_value = np.max(self.Q[state][:,:,0])
        player1_value = np.max(self.Q[state][:,:,1])

        return player0_value, player1_value

class QLearning(BaseAgentLearner):
    def __init__(self, soccer_env):
        super().__init__()

        self.Q = {}
        for s in soccer_env.states:
            self.Q[s] = np.zeros((NUM_ACTIONS, 1), dtype=np.float64)

        self.logger = GameStateLogger(SINGLE_ACTION)
        self.file_output = "q-learning"

    def policy_at_log_state(self):
        return self.Q[self.logger.state_logging]

    def value_at_state(self, state):
        # Value is max over all actions at state
        return np.max(self.Q[state])

    def update_Q_values(self, state_s, actions, rewards, state_s_prime):
        # Compute Q values for player 0, ignoring opponent's actions
        player0_action = actions[0]
        q_s_a = self.Q[state_s][player0_action]
        v_s_prime = self.value_at_state(state_s_prime)

        # Update estimate of Q(s,a)
        q_s_a_update = (1.0 - self.alpha) * q_s_a + \
                    self.alpha * ((1.0 - self.gamma) * rewards[0] + self.gamma * v_s_prime)

        self.Q[state_s][player0_action] = q_s_a_update

        return q_s_a_update

def play_episode(soccer_env, learner):
    # Set up initial state of game
    soccer_env.player_positions = random.sample(soccer_env.initial_states, 2)
    soccer_env.player_with_ball = np.random.randint(NUM_PLAYERS)

    while True:
        learner.logger.num_iterations += 1

        # Select random order of the two players
        player_order = np.random.choice(range(NUM_PLAYERS), NUM_PLAYERS, replace=False)
        # Select random actions for the two players
        player_actions = np.random.randint(low=0, high=NUM_ACTIONS, size=NUM_PLAYERS)

        # State before actions
        state_s = soccer_env.get_game_state()
        action_state = soccer_env.get_actions_per_player(player_order, player_actions)

        for player, action in zip(player_order, player_actions):
            soccer_env.attempt_move(player, action)
            turn_rewards = soccer_env.get_reward_state()
            if any(turn_rewards):
                break

        # State after actions
        state_s_prime = soccer_env.get_game_state()

        # Update Q-values: s, a, r, s'
        q_value = learner.update_Q_values(state_s, action_state, turn_rewards, state_s_prime)
        learner.decay_alpha()

        # End of turn observation
        learner.logger.add_observation(q_value, state_s, action_state)

        # End of episode
        if any(turn_rewards):
            break

if __name__ == "__main__":
    solvers.options["show_progress"] = False
    # Silent output for Foe-Q learning
    solvers.options['glpk'] = {'msg_lev': 'GLP_MSG_OFF'}

    soccer_env = SoccerEnvironment()

    learners = [CorrelatedQ(soccer_env), FoeQ(soccer_env), FriendQ(soccer_env),
                QLearning(soccer_env)]
    # learners = [QLearning(soccer_env)]
    # learners = [FriendQ(soccer_env)]
    # learners = [FoeQ(soccer_env)]
    # learners = [CorrelatedQ(soccer_env)]

    np.random.seed(1729)

    for learner in learners:
        i = 0
        print("Start learner: {}".format(learner.file_output))
        start = time.time()
        while learner.logger.num_iterations < NUM_ITERATIONS:
            i += 1
            if i % 50000 == 0:
                print("Iteration: {}".format(learner.logger.num_iterations))
            play_episode(soccer_env, learner)

        end = time.time()
        print("Runtime for {} learner: {}s".format(learner.file_output, end-start))

        q_values_diff = np.abs(np.ediff1d(learner.logger.q_values))
        learner_plot_data = np.column_stack((learner.logger.iterations[1:], q_values_diff))
        np.save(learner.file_output + ".npy", learner_plot_data)

        # print("Final Policy for Player A at log state:\n{}".format(
        #                                     learner.policy_at_log_state()))
